package com.onespan.authentication.service;

import com.onespan.authentication.service.client.OASProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerApplication {

	public static void main(String[] args) {
		OASProperties.getInstance();
		SpringApplication.run(ServerApplication.class, args);
	}

}
