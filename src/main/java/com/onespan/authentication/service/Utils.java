package com.onespan.authentication.service;

import com.onespan.authentication.service.client.OASNotFoundException;
import com.vasco.identikey.controller.IdentikeyError;
import com.vasco.identikey.controller.IdentikeyResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Utils {

    final static private Logger logger = LogManager.getLogger(Utils.class);

    public static boolean isEmpty(String value) {
        return value == null || value.equals("");
    }

    static public void checkErrorIdentikeyResponse(IdentikeyResponse response, boolean isQuery) throws Exception {
        if(response.getReturnCode() != 0) {
            switch (response.getReturnCode()) {
                case -13:
                    throw new OASNotFoundException();
                default:
                    if(isQuery) {
                        logger.error("Status Code: " + response.getStatusCode());
                        for(IdentikeyError error : response.getErrorStack())
                            logger.error("Error Code: " + error.getErrorCode() + " Error Message: " + error.getErrorMessage());
                    } else
                        printError(response);
                    throw new Exception("Authentication Server returned an unexpected response");
            }
        }
    }

    static public void printError(IdentikeyResponse response) {
        logger.error("Status Code: " + response.getStatusCode() + " Return Code: " + response.getReturnCode());
        for(IdentikeyError error : response.getErrorStack())
            logger.error("Error Code: " + error.getErrorCode() + " Error Message: " + error.getErrorMessage());
    }
}
