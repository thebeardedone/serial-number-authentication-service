package com.onespan.authentication.service;

import com.onespan.authentication.service.client.OASClient;
import com.onespan.authentication.service.client.OASNotFoundException;
import com.onespan.authentication.service.client.OASProperties;
import com.onespan.authentication.service.response.GenericResponse;
import com.onespan.authentication.service.response.ResponseFactory;
import com.vasco.identikey.controller.authentication.AuthenticationCommandResponse;
import com.vasco.identikey.model.Digipass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Authentication {

    final static private Logger logger = LogManager.getLogger(Authentication.class);

    @PostMapping("/authUser")
    public ResponseEntity<GenericResponse> execute(@RequestBody AuthenticationRequest authenticationRequest) {

        logger.debug("received authentication request");
        if (Utils.isEmpty(authenticationRequest.getSerialNumber()) || authenticationRequest.getSerialNumber().contains("*") || Utils.isEmpty(authenticationRequest.getOtp()) || !authenticationRequest.getOtp().matches("[0-9]+")) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseFactory.CreateInvalidDataResponse());
        }
        logger.debug("validated authentication request parameters");

        if(OASProperties.getInstance() == null) {
            logger.error("failed to retrieve OAS properties");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ResponseFactory.CreateInternalServerError());
        }

        OASClient oasClient = OASClient.getInstance();
        if(oasClient == null) {
            logger.error("Failed to configure OAS client");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ResponseFactory.CreateInternalServerError());
        }

        if(!oasClient.isAdminSessionAlive()) {
            logger.debug("attempting OAS admin logon");
            boolean success = oasClient.performAdminLogon();
            if (!success) {
                logger.debug("failed to perform admin logon");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ResponseFactory.CreateInternalServerError());
            }
            logger.debug("successfully performed OAS admin logon");
        }

        Digipass digipass;

        try {
            logger.debug("attempting to query DIGIPASS with serial number: " + authenticationRequest.getSerialNumber());
            digipass = oasClient.getDigipass(authenticationRequest.getSerialNumber());
            logger.debug("successfully retrieved DIGIPASS with serial number: " + authenticationRequest.getSerialNumber());
        } catch (OASNotFoundException exception) {
            logger.warn("failed to find DIGIPASS with serial number: " + authenticationRequest.getSerialNumber());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ResponseFactory.CreateNotFoundError());
        } catch (Exception exception) {
            logger.error("failed to query DIGIPASS with serial number: " + authenticationRequest.getSerialNumber());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ResponseFactory.CreateInternalServerError());
        }

        if(Utils.isEmpty(digipass.getAssignedUserID())) {
            logger.warn("retrieved DIGIPASS is not assigned to a user");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseFactory.CreateInvalidDataResponse());
        }

        logger.debug("attempting authUser for user: " + digipass.getAssignedUserID());
        AuthenticationCommandResponse authenticationCommandResponse = oasClient.performAuthUser(digipass.getDomain(), digipass.getAssignedUserID(), authenticationRequest.getOtp());
        logger.debug("successfully performed authUser for user: " + digipass.getAssignedUserID());

        return ResponseEntity.status(HttpStatus.OK).body(ResponseFactory.CreateAuthenticationResponse(authenticationCommandResponse));
    }
}
