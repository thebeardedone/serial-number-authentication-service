package com.onespan.authentication.service.client;

import com.onespan.authentication.service.Utils;
import com.vasco.identikey.controller.ConfigurationBean;
import com.vasco.identikey.controller.administration.AdministrationBean;
import com.vasco.identikey.controller.administration.AdministrationCommandResponse;
import com.vasco.identikey.controller.administration.handler.digipass.DigipassQueryResponse;
import com.vasco.identikey.controller.administration.handler.digipass.DigipassSearchParams;
import com.vasco.identikey.controller.authentication.AuthenticationBean;
import com.vasco.identikey.controller.authentication.AuthenticationCommandResponse;
import com.vasco.identikey.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OASClient {

    private static OASClient instance;
    final static private Logger logger = LogManager.getLogger(OASClient.class);

    private ConfigurationBean configurationBean;
    private AdministrationBean administrationBean;

    public OASClient(String soapURL, String authenticationComponent) {
        configurationBean = new ConfigurationBean();
        configurationBean.setPrimarySoapURL(soapURL);
        configurationBean.setAuthenticationComponent(authenticationComponent);
        administrationBean = new AdministrationBean(configurationBean);
    }

    public static OASClient getInstance() {
        if(instance == null) {
            if(OASProperties.getInstance() == null)
                return null;
            ServerDetails serverDetails = OASProperties.getInstance().getServerDetails();
            instance = new OASClient(serverDetails.getUrl(), serverDetails.getAuthenticationComponent());
        }
        return instance;
    }

    boolean performAdminLogon(String domain, String userId, String password) {
        AdministrationCommandResponse administrationCommandResponse = administrationBean.logon(domain, userId, null, null, password, Credentials.RequestHostCode.No);
        if(administrationCommandResponse.getStatusCode() != 0 || administrationCommandResponse.getReturnCode() != 0) {
            logger.error("Failed to perform administrative logon for userId: " + userId);
            Utils.printError(administrationCommandResponse);
            return false;
        }
        return true;
    }

    public boolean isAdminSessionAlive() {
        return administrationBean.isSessionAlive();
    }

    public boolean performAdminLogon(ServerDetails serverDetails) {
        return performAdminLogon(serverDetails.getDomain(), serverDetails.getUserId(), serverDetails.getPassword());
    }

    public boolean performAdminLogon() {
        return performAdminLogon(OASProperties.getInstance().getServerDetails());
    }

    public Digipass getDigipass(String serialNumber) throws Exception {
        DigipassSearchParams digipassSearchParams = new DigipassSearchParams();
        digipassSearchParams.setSerialNumber(serialNumber);
        DigipassQueryResponse digipassQueryResponse = administrationBean.getDigipassHandler().search(digipassSearchParams);
        Utils.checkErrorIdentikeyResponse(digipassQueryResponse, true);
        if(digipassQueryResponse.getResults().size() == 0)
            throw new OASNotFoundException();
        return digipassQueryResponse.getResults().get(0);
    }

    public AuthenticationCommandResponse performAuthUser(String domain, String userID, String otp) {
        return new AuthenticationBean(configurationBean).authUser(domain, userID, "", otp, "", Credentials.RequestHostCode.No);
    }
}
