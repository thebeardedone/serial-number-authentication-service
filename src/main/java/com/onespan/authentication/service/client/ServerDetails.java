package com.onespan.authentication.service.client;

public class ServerDetails {

    private String url;
    private String domain;
    private String userId;
    private String password;

    private String authenticationComponent;

    public ServerDetails() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthenticationComponent() {
        return authenticationComponent;
    }

    public void setAuthenticationComponent(String authenticationComponent) {
        this.authenticationComponent = authenticationComponent;
    }
}
