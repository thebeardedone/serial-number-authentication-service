package com.onespan.authentication.service.client;

import com.onespan.authentication.service.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class OASProperties {

    final static private Logger logger = LogManager.getLogger(OASProperties.class);

    private static OASProperties instance;

    private ServerDetails serverDetails = new ServerDetails();

    private OASProperties(String path) throws IOException, IllegalArgumentException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(path));

        loadServerDetails(properties, serverDetails);
    }

    public static OASProperties getInstance() {
        if(instance == null) {
            try {
                String path = System.getProperty("com.onespan.server.config.file");

                if(path == null) {
                    logger.error("com.onespan.server.config.file must be set");
                    return null;
                }

                logger.info("attempting to read properties file: " + path);
                instance = new OASProperties(path);
                logger.info("successfully read properties file: " + path);
            } catch (IOException e) {
                logger.error("failed to read properties file");
                e.printStackTrace();
                return null;
            }
        }
        return instance;
    }

    private void loadServerDetails(Properties properties, ServerDetails serverDetails) throws IllegalArgumentException {
        String url = properties.getProperty("server.url");
        if(Utils.isEmpty(url))
            throw new IllegalArgumentException("server url is mandatory");

        if(!url.startsWith("http://") && !url.startsWith("https://"))
            throw new IllegalArgumentException("URL must start with http:// or https://");
        serverDetails.setUrl(url);

        String userId = properties.getProperty("server.admin.userid");
        if(Utils.isEmpty(userId))
            throw new IllegalArgumentException("admin userId is mandatory");
        serverDetails.setUserId(userId);

        String domain = properties.getProperty("server.admin.domain");
        if(Utils.isEmpty(domain))
            throw new IllegalArgumentException("admin domain is mandatory");
        serverDetails.setDomain(domain);

        String password = properties.getProperty("server.admin.password");
        if(Utils.isEmpty(password))
            throw new IllegalArgumentException("admin password is mandatory");
        serverDetails.setPassword(password);

        String authenticationComponent = properties.getProperty("server.authentication.component");
        if(Utils.isEmpty(authenticationComponent))
            throw new IllegalArgumentException("authentication component is mandatory");
        serverDetails.setAuthenticationComponent(authenticationComponent);
    }

    public ServerDetails getServerDetails() {
        return serverDetails;
    }
}
