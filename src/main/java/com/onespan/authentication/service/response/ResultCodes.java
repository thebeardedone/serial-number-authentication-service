package com.onespan.authentication.service.response;

public class ResultCodes {

    private int returnCode;
    private int statusCode;

    ResultCodes() {}

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
