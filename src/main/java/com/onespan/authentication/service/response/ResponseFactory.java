package com.onespan.authentication.service.response;

import com.vasco.identikey.controller.authentication.AuthenticationCommandResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ResponseFactory {

    final static private Logger logger = LogManager.getLogger(ResponseFactory.class);

    public static GenericResponse CreateInvalidDataResponse() {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.getResultCodes().setReturnCode(-1);
        genericResponse.getResultCodes().setStatusCode(-17);

        return genericResponse;
    }

    public static GenericResponse CreateInternalServerError() {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.getResultCodes().setReturnCode(-1);
        genericResponse.getResultCodes().setStatusCode(-400);

        return genericResponse;
    }

    public static GenericResponse CreateNotFoundError() {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.getResultCodes().setReturnCode(-1);
        genericResponse.getResultCodes().setStatusCode(-13);

        return genericResponse;
    }

    public static GenericResponse CreateAuthenticationResponse(AuthenticationCommandResponse authenticationCommandResponse) {
        GenericResponse authenticationResponse = new GenericResponse();
        authenticationResponse.getResultCodes().setReturnCode(authenticationCommandResponse.getReturnCode());
        authenticationResponse.getResultCodes().setStatusCode(authenticationCommandResponse.getStatusCode());

        if(authenticationResponse.getResultCodes().getReturnCode() != 0 && authenticationResponse.getResultCodes().getStatusCode() != 0)
            logger.error("Status Code: " + authenticationResponse.getResultCodes().getStatusCode() + " Return Code: " + authenticationResponse.getResultCodes().getReturnCode());

        return authenticationResponse;
    }
}
