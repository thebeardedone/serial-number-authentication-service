package com.onespan.authentication.service.response;

public class GenericResponse {
    private ResultCodes resultCodes = new ResultCodes();

    public GenericResponse() {}

    public ResultCodes getResultCodes() {
        return resultCodes;
    }

    public void setResultCodes(ResultCodes resultCodes) {
        this.resultCodes = resultCodes;
    }
}
